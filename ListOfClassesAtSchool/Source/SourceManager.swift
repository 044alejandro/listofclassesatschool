//
//  SourceManager.swift
//  ListOfClassesAtSchool
//
//  Created by Виктор Сирик on 25.10.2021.
//

import Foundation
import UIKit

class SourceManager {
    
    func setClasses() -> [Class] {
        let class1 = Class(nameClass: .a1, imageClass: "a1", numberClass: .first)
        let class2 = Class(nameClass: .b1, imageClass: "b1", numberClass: .first)

        let class3 = Class(nameClass: .a2, imageClass: "a2", numberClass: .second)
        let class4 = Class(nameClass: .b2, imageClass: "b2", numberClass: .second)
        let class5 = Class(nameClass: .v2, imageClass: "v2", numberClass: .second)

        let class6 = Class(nameClass: .a3, imageClass: "a3", numberClass: .third)
        let class7 = Class(nameClass: .b3, imageClass: "b3", numberClass: .third)
        let class8 = Class(nameClass: .v3, imageClass: "v3", numberClass: .third)

        let class9 = Class(nameClass: .a4, imageClass: "a4", numberClass: .fourth)
        let class10 = Class(nameClass: .b4, imageClass: "b4", numberClass: .fourth)
        return [class1, class2, class3, class4, class5, class6, class7, class8, class9, class10]
    }
    
    func setStudent() -> [Student] {
        var arrayStudents: [Student] = []
        
        arrayStudents.append(Student(name: "Полина", surname: "Петрова", className: .a1))
        arrayStudents.append(Student(name: "Милана", surname: "Морозова", className: .a1))
        arrayStudents.append(Student(name: "Леонид", surname: "Антонов", className: .a1))
        arrayStudents.append(Student(name: "Виктория", surname: "Александрова", className: .a1))
        arrayStudents.append(Student(name: "Кристина", surname: "Владимирова", className: .b1))
        arrayStudents.append(Student(name: "Кирилл", surname: "Богданов", className: .b1))
        arrayStudents.append(Student(name: "Полина", surname: "Ершова", className: .b1))
        arrayStudents.append(Student(name: "Александр", surname: "Гусеев", className: .a2))
        arrayStudents.append(Student(name: "Михаил", surname: "Королев", className: .a2))
        arrayStudents.append(Student(name: "Святослав", surname: "Фокин", className: .a2))
        arrayStudents.append(Student(name: "Дарья", surname: "Михайлова", className: .b2))
        arrayStudents.append(Student(name: "Ольга", surname: "Яковлева", className: .b2))
        arrayStudents.append(Student(name: "Петр", surname: "Лазарев", className: .b2))
        arrayStudents.append(Student(name: "Иван", surname: "Сидоров", className: .b2))
        arrayStudents.append(Student(name: "Кирилл", surname: "Попов", className: .v2))
        arrayStudents.append(Student(name: "Агата", surname: "Иванова", className: .v2))
        arrayStudents.append(Student(name: "София", surname: "Чернышева", className: .v2))
        arrayStudents.append(Student(name: "Илья", surname: "Лукьянов", className: .a3))
        arrayStudents.append(Student(name: "Артемий", surname: "Калинин", className: .a3))
        arrayStudents.append(Student(name: "Кирилл", surname: "Исаков", className: .a3))
        arrayStudents.append(Student(name: "Полина", surname: "Быкова", className: .b3))
        arrayStudents.append(Student(name: "Гордей", surname: "Емельянов", className: .b3))
        arrayStudents.append(Student(name: "Данила", surname: "Грачев", className: .b3))
        arrayStudents.append(Student(name: "Андрей", surname: "Кравцов", className: .v3))
        arrayStudents.append(Student(name: "Глеб", surname: "Кузнецов", className: .v3))
        arrayStudents.append(Student(name: "Ульяна", surname: "Петрова", className: .v3))
        arrayStudents.append(Student(name: "Илья", surname: "Горбунов", className: .v3))
        arrayStudents.append(Student(name: "Алена", surname: "Титова", className: .a4))
        arrayStudents.append(Student(name: "Олег", surname: "Гурбов", className: .a4))
        arrayStudents.append(Student(name: "Амина", surname: "Горюнова", className: .a4))
        arrayStudents.append(Student(name: "Полина", surname: "Рожкова", className: .a4))
        arrayStudents.append(Student(name: "Анна", surname: "Максимова", className: .b4))
        arrayStudents.append(Student(name: "Ксения", surname: "Кузнецова", className: .b4))
        arrayStudents.append(Student(name: "Максим", surname: "Васильев", className: .b4))
        arrayStudents.append(Student(name: "Алина", surname: "Романова", className: .b4))
        
        return arrayStudents
    }
    
    func findCurrentClass(numberOfClass: ClassNumber) -> [Class] {
        var newArray: [Class] = []
        for item in setClasses() {
            if item.numberClass == numberOfClass {
                newArray.append(item)
            }
        }
        return newArray
    }
    
    func setSections() -> [Sections] {
        let section1 = Sections(title: "Первый класс", logoClass: "first", options: findCurrentClass(numberOfClass: .first), isSelected: false)
        let section2 = Sections(title: "Второй класс", logoClass: "second", options: findCurrentClass(numberOfClass: .second), isSelected: false)
        let section3 = Sections(title: "Третий класс", logoClass: "third", options: findCurrentClass(numberOfClass: .third), isSelected: false)
        let section4 = Sections(title: "Четвертый класс", logoClass: "fourth", options: findCurrentClass(numberOfClass: .fourth), isSelected: false)
        
        return[section1, section2, section3, section4]
    }
    
    func addStudent(arrayWithStudents: [Student], newStudent: Student) -> [Student]{
        var newArray = arrayWithStudents
        newArray.append(newStudent)
        return newArray.sorted(by: {$0.name < $1.name})
    }
    
    func findStudents(type: ClassesName, arrayWithStudents: [Student]) -> [Student] {
        var studentsInCorrectClass: [Student] = []
        for student in arrayWithStudents{
            if student.className == type {
                studentsInCorrectClass.append(student)
            }
        }
        return studentsInCorrectClass
    }
    
    func getAlert(typeError: String) -> UIAlertController {
        let alertVC = UIAlertController(title: "Error", message: typeError, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
        alertVC.addAction(cancelAction)
        return alertVC
    }
    
}
