//
//  AddStudentViewController.swift
//  ListOfClassesAtSchool
//
//  Created by Виктор Сирик on 29.10.2021.
//

import UIKit

class AddStudentViewController: UIViewController {

    @IBOutlet weak var nameStudentTextField: UITextField!
    @IBOutlet weak var surnameStudentTextField: UITextField!
    @IBOutlet weak var classNameStudentTextField: UITextField!
    var arrayOfNamesClasses: [ClassesName] = [.a1, .b1, .a2, .b2, .v2, .a3, .b3, .v3, .a4, .b4]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func addNewStudentAction(_ sender: Any) {
        guard let name = nameStudentTextField.text, let surname = surnameStudentTextField.text, let nameClass = classNameStudentTextField.text, !name.isEmpty, !surname.isEmpty, !nameClass.isEmpty else {
            let alertVC = SourceManager().getAlert(typeError: "Enter fields")
            present(alertVC, animated: true, completion: nil)
            return
        }
        var findClass: Bool = false
        let listOfClassesVC = (self.tabBarController?.viewControllers?[0] as? ListOfClassesViewController)
        for item in arrayOfNamesClasses {
            if item.rawValue == nameClass {
                let newStudent = Student(name: name, surname: surname, className: item)
                listOfClassesVC?.allStudents = SourceManager().addStudent(arrayWithStudents: listOfClassesVC?.allStudents ?? [], newStudent: newStudent)
                findClass = true
                self.tabBarController?.selectedIndex = 0
            }
        }
        if !findClass {
            let alertVC = SourceManager().getAlert(typeError: "Error name class")
            present(alertVC, animated: true, completion: nil)
        }
    }
}
