//
//  ListOfGraduatesViewController.swift
//  ListOfClassesAtSchool
//
//  Created by Виктор Сирик on 30.10.2021.
//

import UIKit

class ListOfGraduatesViewController: UIViewController {

    @IBOutlet weak var listOfGraduatesTableView: UITableView!
    var graduates: [Student] = []
    var studentsCellID = "StudentCell"

    
    override func viewDidLoad() {
        super.viewDidLoad()

        listOfGraduatesTableView.register(UINib(nibName: studentsCellID, bundle: nil), forCellReuseIdentifier: studentsCellID)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        listOfGraduatesTableView.reloadData()
    }
}
