//
//  ListOfGraduatesViewController + Extensions.swift
//  ListOfClassesAtSchool
//
//  Created by Виктор Сирик on 30.10.2021.
//

import Foundation
import UIKit

extension ListOfGraduatesViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return graduates.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: studentsCellID, for: indexPath) as! StudentCell
        cell.update(student: graduates[indexPath.row])
        return cell
    }
}
