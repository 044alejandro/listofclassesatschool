//
//  ListOfStudentsViewController.swift
//  ListOfClassesAtSchool
//
//  Created by Виктор Сирик on 29.10.2021.
//

import UIKit

class ListOfStudentsViewController: UIViewController {

    @IBOutlet weak var listOfStudentsTableView: UITableView!
    var students: [Student] = []
    var studentsCellID = "StudentCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        listOfStudentsTableView.register(UINib(nibName: studentsCellID, bundle: nil), forCellReuseIdentifier: studentsCellID)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        listOfStudentsTableView.reloadData()
    }
    
}
