//
//  Student.swift
//  ListOfClassesAtSchool
//
//  Created by Виктор Сирик on 29.10.2021.
//

import Foundation

struct Student {
    let name: String
    let surname: String
    let className: ClassesName
}
