//
//  ListOfStudentsViewController. + Extensions.swift
//  ListOfClassesAtSchool
//
//  Created by Виктор Сирик on 29.10.2021.
//

import Foundation
import UIKit

extension ListOfStudentsViewController: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return students.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: studentsCellID, for: indexPath) as! StudentCell
        cell.update(student: students[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .delete
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete{
            var graduatesArray: [Student] = []
            let graduatesListVC = (self.tabBarController?.viewControllers?[3] as? ListOfGraduatesViewController)
            let classesListVC = (self.tabBarController?.viewControllers?[0] as? ListOfClassesViewController)
            var index = 0
            for item in classesListVC?.allStudents ?? [] {
                if item.surname == students[indexPath.row].surname && item.name == students[indexPath.row].name{
                    classesListVC?.allStudents.remove(at: index)
                    break
                }
                index += 1
            }
            graduatesArray = graduatesListVC?.graduates ?? []
            graduatesArray.append(students[indexPath.row])
            students.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
            graduatesListVC?.graduates = graduatesArray
        
        }
    }
    
}
