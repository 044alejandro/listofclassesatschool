//
//  StudentCell.swift
//  ListOfClassesAtSchool
//
//  Created by Виктор Сирик on 29.10.2021.
//

import UIKit

class StudentCell: UITableViewCell {
    @IBOutlet weak var nameStudentLabel: UILabel!
    @IBOutlet weak var surnameStudentLabel: UILabel!
    @IBOutlet weak var studentClassLabel: UILabel!
    
    func update(student: Student) {
        nameStudentLabel.text = student.name
        surnameStudentLabel.text = student.surname
        studentClassLabel.text = student.className.rawValue
    }
    
}
