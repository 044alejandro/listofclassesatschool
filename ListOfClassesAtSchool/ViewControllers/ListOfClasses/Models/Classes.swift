//
//  Classes.swift
//  ListOfClassesAtSchool
//
//  Created by Виктор Сирик on 25.10.2021.
//

import Foundation
enum ClassesName: String {
case a1 = "1-А"
case b1 = "1-Б"
case a2 = "2-А"
case b2 = "2-Б"
case v2 = "2-В"
case a3 = "3-А"
case b3 = "3-Б"
case v3 = "3-В"
case a4 = "4-А"
case b4 = "4-Б"
}

enum ClassNumber {
case first
case second
case third
case fourth
case defaultNumber
}

struct Class {
    var nameClass: ClassesName
    var imageClass: String
    var numberClass: ClassNumber
}

class Sections {
    let title: String
    let logoClass: String
    let options: [Class]
    var isSelected: Bool
    init(title: String, logoClass: String, options: [Class], isSelected: Bool = false) {
        self.title = title
        self.logoClass = logoClass
        self.options = options
        self.isSelected = isSelected
    }
}
