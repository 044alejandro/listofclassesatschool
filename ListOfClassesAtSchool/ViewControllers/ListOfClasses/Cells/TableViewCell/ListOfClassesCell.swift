//
//  ListOfClassesCell.swift
//  ListOfClassesAtSchool
//
//  Created by Виктор Сирик on 25.10.2021.
//

import UIKit
protocol ShowStudentsInActualClassDelegate {
    func showStudents(index: Int, numberClass: ClassNumber)
}

class ListOfClassesCell: UITableViewCell {

    @IBOutlet weak var classesNameLabel: UILabel!
    @IBOutlet weak var classImage: UIImageView!
    var delegate: ShowStudentsInActualClassDelegate?
    var classNumber: ClassNumber = .defaultNumber
    var sectionNumber: Int = 0

    
    func update(classType: Class) {
        classNumber = classType.numberClass
        classesNameLabel.text = classType.nameClass.rawValue
        classImage.image = UIImage(named: classType.imageClass)
    }
    func updateSection(sectionData: Sections) {
        classesNameLabel.text = sectionData.title
        classImage.image = UIImage(named: sectionData.logoClass)
    }
    
    @IBAction func showStudentsClassAction(_ sender: Any) {
        delegate?.showStudents(index: tag-1, numberClass: classNumber)

    }
    
}
