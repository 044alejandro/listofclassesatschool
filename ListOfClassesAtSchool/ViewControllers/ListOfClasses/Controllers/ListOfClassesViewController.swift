//
//  ListOfClassesViewController.swift
//  ListOfClassesAtSchool
//
//  Created by Виктор Сирик on 25.10.2021.
//

import UIKit

class ListOfClassesViewController: UIViewController {

    @IBOutlet weak var listOfClassesTableView: UITableView!
    var allStudents:[Student] = []
    var firstClass: [Class] = []
    var secondClass: [Class] = []
    var thirdClass: [Class] = []
    var fourthClass: [Class] = []
    
    var sections: [Sections] = []

    
    override func viewDidLoad() {
        super.viewDidLoad()
        allStudents = SourceManager().setStudent()
        sections = SourceManager().setSections()

        listOfClassesTableView.register(UINib(nibName: "ListOfClassesCell", bundle: nil),  forCellReuseIdentifier: "ListOfClassesCell")
        addToArray()
    }
    
    func addToArray() {
        firstClass = SourceManager().findCurrentClass(numberOfClass: .first)
        secondClass = SourceManager().findCurrentClass(numberOfClass: .second)
        thirdClass = SourceManager().findCurrentClass(numberOfClass: .third)
        fourthClass = SourceManager().findCurrentClass(numberOfClass: .fourth)
    }
    
    
}
