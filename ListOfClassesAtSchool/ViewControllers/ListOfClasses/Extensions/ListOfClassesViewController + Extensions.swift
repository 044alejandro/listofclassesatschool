//
//  ListOfClassesViewController + Extensions.swift
//  ListOfClassesAtSchool
//
//  Created by Виктор Сирик on 25.10.2021.
//

import Foundation
import UIKit

extension ListOfClassesViewController: UITableViewDelegate, UITableViewDataSource, ShowStudentsInActualClassDelegate{
   
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if sections[section].isSelected {
            return sections[section].options.count + 1
        } else {
            return 1
        }
    }

    
   func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       tableView.deselectRow(at: indexPath, animated: true)
        sections[indexPath.section].isSelected = !sections[indexPath.section].isSelected
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ListOfClassesCell", for: indexPath) as! ListOfClassesCell
        if indexPath.row == 0{
            cell.updateSection(sectionData: sections[indexPath.section])
        } else {
            cell.update(classType: sections[indexPath.section].options[indexPath.row - 1])
        }
        cell.tag = indexPath.row
        cell.delegate = self
        
        return cell
    }
    
    
    func showStudents(index: Int, numberClass: ClassNumber) {
        let listOfStudentsVC = (self.tabBarController?.viewControllers?[1] as? ListOfStudentsViewController)
        var selectedClass: [Student] = []
        switch numberClass{
        case .first:
            selectedClass = SourceManager().findStudents(type: firstClass[index].nameClass, arrayWithStudents: allStudents)
        case .second:
            selectedClass = SourceManager().findStudents(type: secondClass[index].nameClass, arrayWithStudents: allStudents)
        case .third:
            selectedClass = SourceManager().findStudents(type: thirdClass[index].nameClass, arrayWithStudents: allStudents)
        case .fourth:
            selectedClass = SourceManager().findStudents(type: fourthClass[index].nameClass, arrayWithStudents: allStudents)
        case .defaultNumber:
            return
        }
        listOfStudentsVC?.students = selectedClass
        self.tabBarController?.selectedIndex = 1

    }
    
}
